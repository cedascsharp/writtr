# Writtr
A minimalistic and text editor.


## About

Writtr is a small project I started for fun, to try to make an editor that fits
with my current linux distro ( Elementary OS).

I want to make an editor that stays centered and scrolls when bigger than the
window itself.

I also want to have a keyboard-focused experience that's not in your way.
For example, the treeview will be a drawer that open with a hotkey.
No file menu, no graphical-bloating, just text and keyboard :P


## Screenshot

![Writtr v0.1.0](https://i.imgur.com/IRkNF1I.png)


### Changelog

##### Initial Release - v0.1.0